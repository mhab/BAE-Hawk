This project is about the "BAE Systems Hawk"  advanced jet trainer model for the OpenSource flight-simulator "Flightgear".

Main focus of this project is to provide good visual quality of the model with the main use for "formation flying" in Flightgear. 

Find more info in the gitlab Wiki: https://gitlab.com/mhab/BAE-Hawk/wikis/home

In order to use this aircraft model you need to install Flightgear flight simulator. Flightgear Main Web Site: http://www.flightgear.org/

see also Flightgear Wiki: http://wiki.flightgear.org  
