Readme File for Flightgear 3.4/2016.1 formation upgrade
=======================================================

In order to use the predefined formations as delivered with this model in
Flightgear 3.4/2016.1 you need a Flightgear upgrade, which extends the FG formation support.

--> see more info on formation flight in "Readme_Formations.txt" in AI subfolder.

Installing the Upgrade:
----------------------
The 3 files listed below need modification. You need to copy them from the "FG34_formation_upgrade"
subdirectory to the [FG_HOME]/data subdirectory.

Remark: For convenience the files already reside in the same directory structure as needed
in the Flightgear installation.

1) Aircraft\Generic\formation.nas
---------------------------------
Extend formation handling from 4 to max 10 aircraft.
Use a model specific formation path (if set).
Functions for formation handling dialog added (see below)
Enable menu "Wingman Control" (see below)
Nasal runtime erros fixed.

2) gui\dialogs\formation.xml
----------------------------
Use functions for formation control. Changed property path for dialog.

Remark: In case you use the keyboard shortcuts of the "keyboard_snippet.xml" mentioned below
        and you like to have the shortcuts marked in the wingman dialog you may use
        the "formation_with_shortcuts.xml". Rename it to "formation.xml" for use in FG

3) gui\menubar.xml
------------------
Disable wingman control, if no wingman is there or no formation support is there
(not really necessary, only useful for more consistent handling)

4) Keyboard shortcuts
---------------------
If you like to use keyboard shortcuts, feel free to integrate the "keyboard_snippet.xml" into
the Flightgear keyboard definition file "keyboard.xml".

Remark: Make sure to put the xml-code of the snippet between the <PropertyList></PropertyList>
        tags of "keyboard.xml".
