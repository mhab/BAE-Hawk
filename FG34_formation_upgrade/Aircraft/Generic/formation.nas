#####################################################################################
#                                                                                   #
#  this script runs the formation selection utility                                 #
#                                                                                   #
#####################################################################################

# ================================ Initalize ======================================
# Make sure all needed properties are present and accounted
# for, and that they have sane default values.

for(var i = 0; i < 20; i = i + 1){
  setprop("/sim/model/formation/position[" ~ i ~ "]/x-offset", 0);
  setprop("/sim/model/formation/position[" ~ i ~ "]/y-offset", 0);
  setprop("/sim/model/formation/position[" ~ i ~ "]/z-offset", 0);
}

formation_wingmen_total_Node = props.globals.getNode("sim/model/formation/wingmen_total", 1);
formation_wingmen_total_Node.setIntValue(0);

formation_wingmen_avail_Node = props.globals.getNode("sim/model/formation/wingmen_available", 1);
formation_wingmen_avail_Node.setIntValue(0);

formation_variant_Node = props.globals.getNode("sim/formation/variant", 1);
formation_variant_Node.setIntValue(0);

# flag to indicate formation handling is loaded (dialog, shortcuts, menubar)
formation_valid_Node = props.globals.getNode("sim/formation/valid", 1);
formation_valid_Node.setBoolValue(1);

formation_path_Node = props.globals.getNode("sim/formation/path", 1);
if ( getprop("/sim/formation/path") == nil ) {
  formation_path_Node.setValue("Aircraft/Generic/Formations");
}

###########################
# save_scenario_positions(n)
#   n ... save positions of wingman n
# 999 ... save psotions of all wingmen
###########################
save_scenario_positions = func(n) {

  var wm_count = getprop("/sim/model/formation/wingmen_available");
  for(var i = 0; i < wm_count; i = i + 1) {
    if ( getprop("/ai/models/wingman[" ~ i ~ "]/valid") and ((n == i) or (n == 999)) ) {
      print("save scenario position settings [" ~ i ~ "] ...");
      setprop("/ai/models/wingman[" ~ i ~ "]/position_scenario/tgt-x-offset",getprop("/ai/models/wingman[" ~ i ~ "]/position/tgt-x-offset"));
      setprop("/ai/models/wingman[" ~ i ~ "]/position_scenario/tgt-y-offset",getprop("/ai/models/wingman[" ~ i ~ "]/position/tgt-y-offset"));
      setprop("/ai/models/wingman[" ~ i ~ "]/position_scenario/tgt-z-offset",getprop("/ai/models/wingman[" ~ i ~ "]/position/tgt-z-offset"));
    }
  }

}

set_formation_positions = func() {

  var wm_count = getprop("/sim/model/formation/wingmen_available");
  for(var i = 0; i < wm_count; i = i + 1) {
    if ( getprop("/ai/models/wingman[" ~ i ~ "]/position/tgt-x-offset") != nil ) {
      if ( i == 1 ) print("set formation positions: ", getprop("/sim/model/formation/variant"));
      if ( getprop("/sim/model/formation/position[" ~ i ~ "]/x-offset") != nil ){
        setprop("/ai/models/wingman[" ~ i ~ "]/position/tgt-x-offset",getprop("/sim/model/formation/position[" ~ i ~ "]/x-offset"));
        setprop("/ai/models/wingman[" ~ i ~ "]/position/tgt-y-offset",getprop("/sim/model/formation/position[" ~ i ~ "]/y-offset"));
        setprop("/ai/models/wingman[" ~ i ~ "]/position/tgt-z-offset",getprop("/sim/model/formation/position[" ~ i ~ "]/z-offset"));
      }
    }
  }
}

reset_formation_positions = func() {

  var wm_count = getprop("/sim/model/formation/wingmen_available");
  for(var i = 0; i < wm_count; i = i + 1) {
    if ( getprop("/ai/models/wingman[" ~ i ~ "]/position/tgt-x-offset") != nil ) {
      # reset tgt offset
      if ( i == 1 ) print("reset positions /ai/models/wingman[" ~ i ~ "]/position/tgt-x-offset");
      var propdel_node = props.globals.getNode("/ai/models/wingman[" ~ i ~ "]/position/tgt-x-offset",1);
      propdel_node.remove();
      var propdel_node = props.globals.getNode("/ai/models/wingman[" ~ i ~ "]/position/tgt-y-offset",1);
      propdel_node.remove();
      var propdel_node = props.globals.getNode("/ai/models/wingman[" ~ i ~ "]/position/tgt-z-offset",1);
      propdel_node.remove();

      # reset offset
      if ( i == 1 ) print("reset positions /ai/models/wingman[" ~ i ~ "]/position/x-offset");
      var propdel_node = props.globals.getNode("/ai/models/wingman[" ~ i ~ "]/position/x-offset",1);
      propdel_node.remove();
      var propdel_node = props.globals.getNode("/ai/models/wingman[" ~ i ~ "]/position/y-offset",1);
      propdel_node.remove();
      var propdel_node = props.globals.getNode("/ai/models/wingman[" ~ i ~ "]/position/z-offset",1);
      propdel_node.remove();

      # reset saved scenario
      if ( i == 1 ) print("reset positions /ai/models/wingman[" ~ i ~ "]/position_scenario/tgt-x-offset");
      var propdel_node = props.globals.getNode("/ai/models/wingman[" ~ i ~ "]/position_scenario/tgt-x-offset",1);
      propdel_node.remove();
      var propdel_node = props.globals.getNode("/ai/models/wingman[" ~ i ~ "]/position_scenario/tgt-y-offset",1);
      propdel_node.remove();
      var propdel_node = props.globals.getNode("/ai/models/wingman[" ~ i ~ "]/position_scenario/tgt-z-offset",1);
      propdel_node.remove();
    }
  }
}

restore_scenario_positions = func() {

  print("formation listener: reset to scenario ...");

  var wm_count = getprop("/sim/model/formation/wingmen_available");
  for(var i = 0; i < wm_count; i = i + 1) {
    if ( getprop("/ai/models/wingman[" ~ i ~ "]/position_scenario/tgt-x-offset") != nil ) {
      setprop("/ai/models/wingman[" ~ i ~ "]/position/tgt-x-offset",getprop("/ai/models/wingman[" ~ i ~ "]/position_scenario/tgt-x-offset"));
      setprop("/ai/models/wingman[" ~ i ~ "]/position/tgt-y-offset",getprop("/ai/models/wingman[" ~ i ~ "]/position_scenario/tgt-y-offset"));
      setprop("/ai/models/wingman[" ~ i ~ "]/position/tgt-z-offset",getprop("/ai/models/wingman[" ~ i ~ "]/position_scenario/tgt-z-offset"));
    }
  }

}

reset = func() {
  print("reset formation position settings ...");
  reset_formation_positions();
  # reload of scenario is necessary
  # unclear how to do
  print("scenario must be reloaded ");
  gui.popupTip("Scenario must be reloaded manually !");
}

init_formation = func() {

  if ( !getprop("/sim/formation/override") and !getprop("/sim/formation/delayed") ) {
    var wm_count = check_wingmen();
    for(var i = 0; i < wm_count; i = i + 1) {
      if ( getprop("/ai/models/wingman[" ~ i ~ "]/valid") ) {
        setprop("ai/models/wingman[" ~ i ~ "]/position/tgt-x-offset",getprop("/sim/model/formation/position[" ~ i ~ "]/x-offset"));
        setprop("ai/models/wingman[" ~ i ~ "]/position/tgt-y-offset",getprop("/sim/model/formation/position[" ~ i ~ "]/y-offset"));
        setprop("ai/models/wingman[" ~ i ~ "]/position/tgt-z-offset",getprop("/sim/model/formation/position[" ~ i ~ "]/z-offset"));
      }
    }
  }
}

#########################
# check if at least 1 wingman is available
# and if num of wingmen changed then
#  - enable/disable menu
#  - enable/disable formation
#  - reset formation positions
#########################
check_wingmen = func() {

  var wm_exist = 0;
  var wm_count = 0;

  foreach (var c; props.globals.getNode("/ai/models").getChildren("wingman")) {
    if( c.getValue("valid") ) {
      wm_exist=1;
      wm_count+=1;
    }
  }

  if ( getprop("/sim/model/formation/wingmen_available") != wm_count ) {

    setprop("/sim/model/formation/wingmen_available",wm_count);

    if ( wm_exist ) {
      # enable menu entry
      setprop("/sim/menubar/default/menu[6]/item[2]/enabled",1);
    } else {
      # disable menu entry if no wingman remains
      setprop("/sim/menubar/default/menu[6]/item[2]/enabled",0);

      # reset formation handling without wingman
      setprop("/sim/formation/override",1);
      reset_formation_positions();
      print("no wingman available !");
    }

    # activate formation if all wingmen loaded
print("wingmen count: " ~ wm_count ~ " wingmen total: " ~ getprop("/sim/model/formation/wingmen_total"));
    if ( getprop("/sim/model/formation/wingmen_total") == wm_count ) {
      setprop("/sim/formation/delayed",0);
      setprop("/sim/formation/override",0);
      print("formation is complete ...");
    } else {
      setprop("/sim/formation/override",1);
    }
  }
  return wm_count;
}

#check_scenarios = func() {
#
#  var sc_exist = 0;
#  var sc_count = 0;
#
#  foreach (var c; props.globals.getNode("/sim/ai").getChildren("scenario")) {
#    if( c.getValue() != nil ) {
#      sc_exist=1;
#      sc_count+=1;
#    }
#  }
#  return sc_count;
#}

var formation_dialog = nil;

initialize = func {

  print("Initializing formation ...");

  # initialize dialogs

  # save chosen formation for next FG start
  aircraft.data.add("sim/model/formation/variant");

  formation_dialog = gui.OverlaySelector.new(
    "Select Formation",
    formation_path_Node.getValue(),
    "sim/model/formation/variant",
    "sim/model/formation/index", nil, func(no) {
      formation_variant_Node.setIntValue(no);
      init_formation();
    }
  );

  #set listeners

  # whenever a formation is changed
  setlistener("/sim/model/formation/variant", func {
    check_wingmen();
    if ( !getprop("/sim/formation/delayed") and !getprop("/sim/formation/override") ) {
      # formation handling is ON
      gui.popupTip("Target Formation: " ~ getprop("/sim/model/formation/variant"));
      set_formation_positions();
    }
  }, 0, 1);

#  # whenever a scenario is changed
#  var sc_count = check_scenarios();
#  for(var i = 0; i < sc_count; i = i + 1){
#    var listen = "/sim/ai/scenario[" ~ i ~ "]";
#    var li = setlistener(listen, func {
#      var scenario=getprop("/sim/ai/scenario[" ~ i ~ "]");
#      if ( scenario != nil ) {
#        print("scenario: " ~ scenario ~ " loaded ... !");
#        save_scenario_positions(i);
#      } else {
#        print("scenario: " ~ scenario ~ " unloaded ... !");
#      }
#      check_wingmen();
#    }, 0, 1);
#    print("listener: " ~ i ~ "  id: " ~ li);
#  }

  ###################
  # wingmen listeners BEGIN
  ###################
  # whenever wingman 0 is changed
  setlistener("/ai/models/wingman/valid", func {
    if ( getprop("/ai/models/wingman/valid") ) {
      print("wingman loaded ... scenario changed !");
      save_scenario_positions(0);

      # activate formation if all wingmen loaded
      if ( getprop("/sim/model/formation/wingmen_total") == 0 ) {
        setprop("/sim/formation/override",0);
        setprop("/sim/formation/delayed",0);
        print("formation is complete ...");
      }
    }
    check_wingmen();
  }, 0, 1);

  # whenever wingman 1 is changed
  setlistener("/ai/models/wingman[1]/valid", func {
    if ( getprop("/ai/models/wingman[1]/valid") ) {
      print("wingman[1] loaded ... scenario changed !");
      # enable menu entry
      save_scenario_positions(1);

      # activate formation if all wingmen loaded
      if ( getprop("/sim/model/formation/wingmen_total") == 1 ) {
        setprop("/sim/formation/override",0);
        setprop("/sim/formation/delayed",0);
        print("formation is complete ...");
      }
    }
    check_wingmen();
  }, 0, 1);

  # whenever wingman 2 is changed
  setlistener("/ai/models/wingman[2]/valid", func {
    if ( getprop("/ai/models/wingman[2]/valid") ) {
      print("wingman[2] loaded ... scenario changed !");
      # enable menu entry
      save_scenario_positions(2);

      # activate formation if all wingmen loaded
      if ( getprop("/sim/model/formation/wingmen_total") == 2 ) {
        setprop("/sim/formation/override",0);
        setprop("/sim/formation/delayed",0);
        print("formation is complete ...");
      }
    }
    check_wingmen();
  }, 0, 1);

  # whenever wingman 3 is changed
  setlistener("/ai/models/wingman[3]/valid", func {
    if ( getprop("/ai/models/wingman[3]/valid") ) {
      print("wingman[3] loaded ... scenario changed !");
      # enable menu entry
      save_scenario_positions(3);

      # activate formation if all wingmen loaded
      if ( getprop("/sim/model/formation/wingmen_total") == 3 ) {
        setprop("/sim/formation/override",0);
        setprop("/sim/formation/delayed",0);
        print("formation is complete ...");
      }
    }
    check_wingmen();
  }, 0, 1);

  # whenever wingman 4 is changed
  setlistener("/ai/models/wingman[4]/valid", func {
    if ( getprop("/ai/models/wingman[4]/valid") ) {
      print("wingman[4] loaded ... scenario changed !");
      # enable menu entry
      save_scenario_positions(4);

      # activate formation if all wingmen loaded
      if ( getprop("/sim/model/formation/wingmen_total") == 4 ) {
        setprop("/sim/formation/override",0);
        setprop("/sim/formation/delayed",0);
        print("formation is complete ...");
      }
    }
    check_wingmen();
  }, 0, 1);

  # whenever wingman 5 is changed
  setlistener("/ai/models/wingman[5]/valid", func {
    if ( getprop("/ai/models/wingman[5]/valid") ) {
      print("wingman[5] loaded ... scenario changed !");
      # enable menu entry
      save_scenario_positions(5);

      # activate formation if all wingmen loaded
      if ( getprop("/sim/model/formation/wingmen_total") == 5 ) {
        setprop("/sim/formation/override",0);
        setprop("/sim/formation/delayed",0);
        print("formation is complete ...");
      }
    }
    check_wingmen();
  }, 0, 1);

  # whenever wingman 6 is changed
  setlistener("/ai/models/wingman[6]/valid", func {
    if ( getprop("/ai/models/wingman[6]/valid") ) {
      print("wingman[6] loaded ... scenario changed !");
      # enable menu entry
      save_scenario_positions(6);

      # activate formation if all wingmen loaded
      if ( getprop("/sim/model/formation/wingmen_total") == 6 ) {
        setprop("/sim/formation/override",0);
        setprop("/sim/formation/delayed",0);
        print("formation is complete ...");
      }
    }
    check_wingmen();
  }, 0, 1);

  # whenever wingman 7 is changed
  setlistener("/ai/models/wingman[7]/valid", func {
    if ( getprop("/ai/models/wingman[7]/valid") ) {
      print("wingman[7] loaded ... scenario changed !");
      # enable menu entry
      save_scenario_positions(7);

      # activate formation if all wingmen loaded
      if ( getprop("/sim/model/formation/wingmen_total") == 7 ) {
        setprop("/sim/formation/override",0);
        setprop("/sim/formation/delayed",0);
        print("formation is complete ...");
      }
    }
    check_wingmen();
  }, 0, 1);

  # whenever wingman 8 is changed
  setlistener("/ai/models/wingman[8]/valid", func {
    if ( getprop("/ai/models/wingman[8]/valid") ) {
      print("wingman[8] loaded ... scenario changed !");
      # enable menu entry
      save_scenario_positions(8);

      # activate formation if all wingmen loaded
      if ( getprop("/sim/model/formation/wingmen_total") == 8 ) {
        setprop("/sim/formation/override",0);
        setprop("/sim/formation/delayed",0);
        print("formation is complete ...");
      }
    }
    check_wingmen();
  }, 0, 1);

  # whenever wingman 9 is changed
  setlistener("/ai/models/wingman[9]/valid", func {
    if ( getprop("/ai/models/wingman[9]/valid") ) {
      print("wingman[9] loaded ... scenario changed !");
      # enable menu entry
      save_scenario_positions(9);

      # activate formation if all wingmen loaded
      if ( getprop("/sim/model/formation/wingmen_total") == 9 ) {
        setprop("/sim/formation/override",0);
        setprop("/sim/formation/delayed",0);
        print("formation is complete ...");
      }
    }
    check_wingmen();
  }, 0, 1);
  ###################
  # wingmen listeners END
  ###################

} # end func

###
# ====================== end Initialization ========================================
###


# Fire it up

setlistener("/sim/signals/fdm-initialized", func {
  initialize();
  save_scenario_positions(999);
  check_wingmen();
}, 0, 1);

# whenever total of wingmen changes
# seems to be necessary here because
# formations are loaded quite late ...
setlistener("/sim/model/formation/wingmen_total", func {
  check_wingmen();
}, 0, 1);

# end

##########################################
# handling of "AI Wingman Controls" dialog
##########################################
toggle_formation_delayed = func() {

  var p = getprop("/sim/formation/delayed");
  setprop("/sim/formation/delayed",!p);

  if ( !p ) {
    screen.log.write("Formation Transition delayed");

  } else {
    screen.log.write("Formation Transition immediate");
    gui.popupTip("Target Formation: " ~ getprop("/sim/model/formation/variant"));

    if ( !getprop("/sim/formation/override") ) {
      # formationhandling is ON
      set_formation_positions();
    } else {
      # formationhandling is OFF
      restore_scenario_positions();
    }
  }

}

toggle_formation_override = func() {

  var p = getprop("/sim/formation/override");
  setprop("/sim/formation/override",!p);

  if ( p ) {
    setprop("/sim/formation/delayed",0);
    screen.log.write("Formation Control enabled, Transitions immediate");
    gui.popupTip("Target Formation: " ~ getprop("/sim/model/formation/variant"));

    # formationhandling is ON
    set_formation_positions();
  } else {
    setprop("/sim/formation/delayed",0);
    screen.log.write("Formation Control disabled, Transitions delayed");

    # formationhandling is OFF
    restore_scenario_positions();
  }

}
