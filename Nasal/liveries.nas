aircraft.livery.init("Aircraft/BAE-Hawk/Models/liveries");

# mhab added
toggle_colortank = func () {
  if ( getprop("/sim/weight[3]/selected") == "Color Tank" ) {
    setprop("/sim/weight[3]/selected","none");
    setprop("/consumables/fuel/tank[5]/selected",0);
    # update weight
    gui.setWeightOpts();
    #setprop("/consumables/fuel/tank[5]/level-gal_us",0);
  } else {
    if ( getprop("/sim/weight[3]/selected") == "none" ) {
      setprop("/sim/weight[3]/selected","Color Tank");
      # update tanks
      setprop("/consumables/fuel/tank[5]/selected",1);
      # update weight
      gui.setWeightOpts();
      setprop("/consumables/fuel/tank[5]/level-gal_us",getprop("/consumables/fuel/tank[5]/capacity-gal_us"));
    }
  }
}
