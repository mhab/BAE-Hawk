## Smoke ##

SmokeParticles = func(step) {
  var smoke=props.globals.getNode("/smoke");
  smoke.getChild("particlepersec").setValue(smoke.getChild("particlepersec").getValue()+step);
  if(smoke.getChild("particlepersec").getValue()<0) {
    smoke.getChild("particlepersec").setValue(0);
  }
  # mhab
  if(smoke.getChild("particlepersec").getValue()>100) {
    smoke.getChild("particlepersec").setValue(100);
  }
  i=0;

  gui.popupTip(sprintf("Smoke: %d%%",smoke.getChild("particlepersec").getValue()));
}
